
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_COMMON_H
#define RIVEN_COMMON_H

#include <string>

#ifdef OS_WINDOWS
# include <windows.h>
#endif

#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <iostream>

namespace riven {

/**
 * Converts a big endian uint32_t to whatever is native for the currently
 * running cpu.
 */
uint32_t big_to_native (uint32_t i);
uint16_t big_to_native (uint16_t in);

inline std::string toLower (const std::string &in) {
	std::string result = in;
	for (unsigned int i=0; i<result.length(); i++)
		if ((result[i] >= 'A') && (result[i] <= 'Z'))
			result[i] += 0x20;
	return result;
}


inline char *toLower (char *in) {
	for (unsigned i=0; in[i]; i++) {
		if ((in[i] >= 'A') && (in[i] <= 'Z'))
			in[i] += 0x20;
	}
	return in;
}

struct Coor {
	short x, y;
	Coor (short _x, short _y) : x(_x), y(_y) {}
	Coor () {}
};

struct nonrecoverable_error {

};

}

#endif

