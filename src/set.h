
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RIVEN_SET_H
#define RIVEN_SET_H

#include <vector>
#include <string>

namespace riven {

namespace set {

// does everything to ensure that get_stack_files will find the actual
// mohawks. Prompts the user to insert a disk, installs the files, etc.
void init ();


// returns a vector of the paths for all the mohawks for this stack.
// The will be accessible, and directly opened by fopen, etc
void get_stack_files (unsigned stack, std::vector<std::string> &files);

// the same, but for all the sound mohawks for this stack
void get_stack_sound_files (unsigned stack, std::vector<std::string> &files);

}

}

#endif


