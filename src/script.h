
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_SCRIPT_H
#define RIVEN_SCRIPT_H

#include <map>
#include <list>

#include "common.h"
#include "file.h"

namespace riven {


class handler_t {

public:

	enum event_t {
		MouseDown = 0,
		MouseUp = 2,
		MouseWithin = 4,
		MouseLeave = 5,
		CardLoad = 6,
		CardClose = 7,
		CardOpen = 9,
		DisplayUpdate = 10
	};

private:

	event_t event;
	uint16_t *cmdList;
	// we don't know how many shorts long a handler is, so
	// we find it for each segment, and save it in a cache.
	std::map<int, uint16_t> cmdLength;

	int findSegmentSize (int offset);

	void runSegment (int addr);
	void runCmd (uint16_t *cmd);

public:

	handler_t (uint16_t *cmdListIn);
	void run () {
		runSegment (1);
	}

	uint16_t getLength () { // number of shorts
		return cmdLength.find(1)->second + 1;
	}

	event_t getEventType () {
		return event;
	}

};


class script_t {

	uint16_t *cmdList;
	std::list<handler_t> handlers;

public:

	script_t (file_t *fileIn, int start);
	~script_t () { delete cmdList; }

	void run_handler (handler_t::event_t event);
	int get_size ();

};

}

#endif

