
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <assert.h>


#include "mohawk.h"

using namespace riven;

/*********** file_table_t ***********/

file_table_t::file_table_t (uint8_t* tableIn) {
	table = tableIn;
	count = big_to_native (*(uint32_t*) table);
}

uint32_t file_table_t::getSize (int index) {
	if (index == -1)
		return 0;

	index--;
	assert ((uint32_t) index <  count);

	uint32_t size = *(uint32_t*) (table + 4 + (index * 10) + 4);
	return ((size & 0xff0000) | ((size & 0xff) << 8) | ((size & 0xff00) >> 8));
}


uint32_t file_table_t::getOffset (int index) {
	if (index == -1)
		return 0;
	
	if ((unsigned) index > count)
		return 0;

	index--;

	return big_to_native (*(uint32_t*) (table + 4 + (index * 10)));
}


/************ resource_t **********/



resource_t::resource_t (uint8_t *resourceDirIn, uint32_t typeIn) {
	resourceDir = resourceDirIn;
	type = typeIn;

	int typeCount = big_to_native (*(uint16_t*) (resourceDir + 2));
	int i;
	for (i=0; i<typeCount; i++) {
		if (*(uint32_t*) (resourceDir + 4 + i*8) == type) {
			resourceTable = resourceDir + big_to_native (*(uint16_t*) (resourceDir + 4 + i*8 + 4));
				
			count = big_to_native (*(uint16_t*) (resourceTable));

			return;
		}
	}
	
	std::cout << "mohawk_t: resource type " << type << " was not found\n";
	resourceTable = 0;
	count = 0;
	throw nonrecoverable_error ();

}

int resource_t::getFileIndex (int id) {
	int i;
	for (i=0; i<count; i++)
		if (id == big_to_native (*(uint16_t*) (resourceTable + 2 + 4*i)))
			return big_to_native (*(uint16_t*) (resourceTable + 2 + 4*i + 2));


	return -1;
}


/************ mohawk_t ***********/

mohawk_t::mohawk_t (const std::string &fileIn) {

	file_name = fileIn;

	file_mmap = mmap_file (fileIn);
	file = (uint8_t*) file_mmap->data;

	if (*(uint32_t*) file != 0x4b57484d) // MHWK
		return;
	
	resourceDir = file + big_to_native (*(uint32_t*) (file + 20));
	fileTable = new file_table_t (resourceDir + big_to_native (*(uint16_t*) (file + 24)));


}

mohawk_t::~mohawk_t () {
	delete fileTable;
	munmap_file (file_mmap);
}

uint8_t* mohawk_t::get_file (uint32_t type, int id) {

	int offset = get_file_loc (type, id);
	return (offset) ? file + offset : NULL;

}

uint32_t mohawk_t::get_file_loc (uint32_t type, int id) {

	if (resources.find (type) == resources.end())
		resources.insert (std::make_pair (type, resource_t (resourceDir, type)));
	
	if (resources.find (type) == resources.end())
		return 0;

	return fileTable->getOffset (resources.find(type)->second.getFileIndex (id));

}

int mohawk_t::get_size (uint32_t type, int id) {

	if (resources.find (type) == resources.end())
		resources.insert (std::make_pair (type, resource_t (resourceDir, type)));

	return fileTable->getSize (resources.find(type)->second.getFileIndex (id));

}





