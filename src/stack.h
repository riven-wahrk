
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_STACK_H
#define RIVEN_STACK_H

#include <vector>

#include <stdint.h>
#include <stdio.h>

/*
 *
 * Manages a stack (each island is its own stack)
 *
 * a - main menu
 * b - boiler/lake
 * g - garden island - has large map/where you locate the domes
 * j - has the community, lake in the middle, submarine, etc
 * o - ghens office - the sky looks red, your in the cage
 * p - prison, where whats-her-name is trapped
 * r - rebels, the beehive thing in the middle of the lake
 * t - temple, telescope, lake in the building
 *
 */

namespace riven {

class mohawk_t;
class file_t;

class mhk_group_t {

	friend class file_t;

	std::vector<mohawk_t*> mohawks;

public:

	mhk_group_t (const std::string &path);
	mhk_group_t () {}

	void load (std::vector<std::string> &files);

};



class stack_t : public mhk_group_t {

	unsigned which;

public:

	mhk_group_t *sound_group ();

	enum { ASPIT, BSPIT, GSPIT, JSPIT, OSPIT, PSPIT, RSPIT, TSPIT };

	static void choose_set ();

	static void load_stack (unsigned which);

};


extern stack_t *game_stack;

}

#endif


