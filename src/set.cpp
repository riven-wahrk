
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "set.h"
#include "stack.h"
#include "system.h"

namespace riven {

struct set_t {

	struct stack_t {

		struct file_t {
			// which disk this is
			unsigned char disk;
			// true if this file needs to be copied to the hard drive
			unsigned char install;
			// the path to the file, from the root of the disk
			const char *path;
		};

		unsigned which;
		file_t *files;
		file_t *sound;
	};

	stack_t *stacks;

};


#define make_set(stacks) { \
(set_t::stack_t[]) { \
stacks \
{ \
0, \
(set_t::stack_t::file_t*) NULL, \
(set_t::stack_t::file_t*) NULL \
}} \
},


#define make_stack(which, files, waves) { \
which, \
(set_t::stack_t::file_t[]) { \
files \
{0, 0, (char*) NULL }}, \
(set_t::stack_t::file_t[]) { \
waves \
{0, 0, (char*) NULL }} \
},

#define make_file(disk,install,path) { disk, install, path },

static set_t *sets = (set_t[]) {


make_set (

	make_stack( stack_t::ASPIT,
		make_file(1, 0, "Data/a_Data.MHK"),
		make_file(1, 0, "ASSETS1/a_Sounds.MHK"))
	make_stack( stack_t::BSPIT,
		make_file(1, 0, "Data/b_Data.MHK"),
		make_file(1, 0, "ASSETS1/b_Sounds.MHK"))
	make_stack (stack_t::GSPIT,
		make_file(1, 0, "Data/g_Data.MHK"),
		make_file(1, 0, "ASSETS1/g_Sounds.MHK"))
	make_stack( stack_t::JSPIT,
		make_file(1, 0, "Data/j_Data1.MHK")
		make_file(1, 0, "Data/j_Data2.MHK"),
		make_file(1, 0, "ASSETS1/j_Sounds.MHK"))
	make_stack( stack_t::OSPIT,
		make_file(1, 0, "Data/o_Data.MHK"),
		make_file(1, 0, "ASSETS1/o_Sounds.MHK"))
	make_stack( stack_t::PSPIT,
		make_file(1, 0, "Data/p_Data.MHK"),
		make_file(1, 0, "ASSETS1/p_Sounds.MHK"))
	make_stack( stack_t::RSPIT,
		make_file(1, 0, "Data/r_Data.MHK"),
		make_file(1, 0, "ASSETS1/r_Sounds.MHK"))
	make_stack( stack_t::TSPIT,
		make_file(1, 0, "Data/t_Data1.MHK")
		make_file(1, 0, "Data/t_Data2.MHK"),
		make_file(1, 0, "Data/t_Sounds.MHK"))
)



{
	(set_t::stack_t*) NULL
}};


static set_t *current_set;


void set::init () {

	current_set = &sets[0];

}


void set::get_stack_files (
		unsigned which_stack,
		std::vector<std::string> &files) {

	// find the set_t::stack_t
	set_t::stack_t *stack = current_set->stacks;

	do {
		if (stack->which == which_stack)
			break;
		stack++;

	} while (stack->files);

	std::vector<std::string> disks;
	list_disks (disks);
	files.clear ();

	for (int i=0; i<disks.size(); i++) {


		for (set_t::stack_t::file_t *file = stack->files; file->path; file++) {

			std::string path = find_file (disks[i], std::string (file->path));
			if (path.length() == 0)
				continue;

			files.push_back (path);

		}

		if (files.size() != 0)
			return;

	}

}


void set::get_stack_sound_files (unsigned stack, std::vector<std::string> &files) {


}




}

