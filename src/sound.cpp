
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Sound.h"

SoundSource::SoundSource (SoundMixer *mixerIn) {

	mixer = mixerIn;
}

SoundSource::~SoundSource () {

	if (mixer)
		mixer->removeSoundSource (this);

}

// from Pete Bernert's spu plugin for psx

SoundMixer::SoundMixer () {
	snd_pcm_hw_params_t *hwparams;
	snd_pcm_sw_params_t *swparams;
	snd_pcm_status_t *status;
	int pspeed = 44100;
	int pchannels = 2;
	int format = SND_PCM_FORMAT_S16_LE;
	int buffer_time = 500000;
	int period_time = buffer_time/5;
	int err;


	if((err=snd_pcm_open(&handle, "default", 
    	SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK))<0) {
		printf("Audio open error: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_nonblock(handle, 0))<0) {
		printf("Can't set blocking moded: %s\n", snd_strerror(err));
		return;
	}

	snd_pcm_hw_params_alloca (&hwparams);
	snd_pcm_sw_params_alloca (&swparams);

	if((err=snd_pcm_hw_params_any (handle, hwparams)) < 0) {
		printf ("Broken configuration for this PCM: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_hw_params_set_access(handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED))<0) {
		printf("Access type not available: %s\n", snd_strerror(err));
		return;
}

	if((err=snd_pcm_hw_params_set_format(handle, hwparams, (snd_pcm_format_t) format))<0) {
		printf("Sample format not available: %s\n", snd_strerror(err));
		return;
}

	if((err=snd_pcm_hw_params_set_channels(handle, hwparams, pchannels))<0) {
		printf("Channels count not available: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_hw_params_set_rate_near(handle, hwparams, (unsigned int*) &pspeed, 0))<0) {
		printf("Rate not available: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_hw_params_set_buffer_time_near(handle, hwparams, (unsigned int*) &buffer_time, 0))<0) {
		printf("Buffer time error: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_hw_params_set_period_time_near(handle, hwparams, (unsigned int*) &period_time, 0))<0) {
		printf("Period time error: %s\n", snd_strerror(err));
		return;
	}

	if((err=snd_pcm_hw_params(handle, hwparams))<0) {
		printf("Unable to install hw params: %s\n", snd_strerror(err));
		return;
	}

	snd_pcm_status_alloca(&status);
	if((err=snd_pcm_status(handle, status))<0) {
		printf("Unable to get status: %s\n", snd_strerror(err));
		return;
	}

	buffer_size=snd_pcm_status_get_avail(status);
}

SoundMixer::~SoundMixer () {
	if (handle) {
		snd_pcm_drop (handle);
		snd_pcm_close (handle);
		handle = NULL;
	}
}


void SoundMixer::mixMono () {
	int count = monoSources.size ();

	int16_t *sources[count];

	int16_t *out = buffer;
	// if there is a sound source that needs to be deleted, its address is
	// put in here.
	SoundSource *remove = NULL;

	int i=0;
	std::vector<SoundSource*>::iterator iter;
	for (iter = monoSources.begin(); iter != monoSources.end(); iter++, i++) {
		sources[i] = NULL;
		//  make sure there is enough data
		while ((*iter)->buffer.size() < 2205) {
			if (!(*iter)->addToBuffer ()) {
				remove = *iter;
			}
		}
		// pop 2205 shorts of data off
		sources[i] = (*iter)->buffer.buffer + (*iter)->buffer.start;
		(*iter)->buffer.start += 2205;
		if ((*iter)->buffer.start == 2205*2*5)
			(*iter)->buffer.start = 0;
	}

	for (i = 0; i<2205; i++) {
		*out = 0;
		for (int j=0; j<count; j++) {
			*out = mix (*out, *sources[j]);
			sources[j]++;
		}
		out[1] = out[2] = out[3] = *out; // mono to stereo
		out += 4;
	}

	if (remove)
		// FIXME leaks w/ more than one source
		delete remove;

}

void SoundMixer::mixStereo () {

	int count = stereoSources.size ();

	int16_t *sources[count];

	int16_t *out = buffer;

	SoundSource *remove = NULL;

	int i=0;
	std::vector<SoundSource*>::iterator iter;
	for (iter = stereoSources.begin(); iter != stereoSources.end(); iter++, i++) {
		sources[i] = NULL;
		//  make sure there is enough data
		while ((*iter)->buffer.size() < 4410) {
			if (!(*iter)->addToBuffer ()) {
				remove = *iter;
				break;
			}
		}
		// pop 2205 shorts of data off
		sources[i] = (*iter)->buffer.buffer + (*iter)->buffer.start;
		(*iter)->buffer.start += 4410;
		if ((*iter)->buffer.start == 2205*2*5)
			(*iter)->buffer.start = 0;
	}

	for (i = 0; i<4410; i++) {
		for (int j=0; j<count; j++) {
			*out = mix (*out, *sources[j]);
			sources[j]++;
		}
		out[1] = *out;
		out += 2;
	}

	if (remove)
		// FIXME leaks w/ more than one source
		delete remove;

}

void SoundMixer::update () {

	snd_pcm_sframes_t frames = snd_pcm_avail_update (handle);

	if (frames < 0)
		return;
	
	if (frames >= 4410) {
		mixMono ();
		mixStereo ();
		snd_pcm_prepare (handle);
		snd_pcm_writei (handle, buffer, 4410);
	}

}


void SoundMixer::removeSoundSource (SoundSource *source) {

	std::vector<SoundSource*>::iterator iter;

	iter = monoSources.begin ();
	while (iter != monoSources.end ()) {
		if (*iter == source)
			iter = monoSources.erase (iter);
		else
			iter++;
	}

	iter = stereoSources.begin ();
	while (iter != stereoSources.end ()) {
		if (*iter == source)
			iter = stereoSources.erase (iter);
		else
			iter++;
	}


}


#if 0
unsigned long Alsa::getBytesBuffered ()
{
 unsigned long l;

 if(handle == NULL)                                 // failed to open?
  return SOUNDSIZE;
 l = snd_pcm_avail_update(handle);
 if(l<0) return 0;
 if(l<buffer_size/2)                                 // can we write in at least the half of fragments?
      l=SOUNDSIZE;                                   // -> no? wait
 else l=0;                                           // -> else go on

 return l;
}

////////////////////////////////////////////////////////////////////////
// FEED SOUND DATA
////////////////////////////////////////////////////////////////////////

void Alsa::feedStreamData(unsigned char* pSound,int lBytes)
{
 if(handle == NULL) return;

 if(snd_pcm_state(handle) == SND_PCM_STATE_XRUN)
  snd_pcm_prepare(handle);
 snd_pcm_writei(handle,pSound,
                iDisStereo == 1 ? lBytes/2 : lBytes/4);
}



#endif

