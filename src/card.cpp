
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include "card.h"
#include "game.h"

using namespace riven;

static std::vector<card_t*> old_cards;
card_t *card_t::current;


card_t::card_t (int idIn) :
	file_t (resource_t::CARD, idIn),
	script (this, 4),
	plst (idIn),
	hotspot (idIn) {

	id = idIn;

	std::cout << "opening new card " << id << "\n";

	current = this;

	script.run_handler (handler_t::CardLoad);
	script.run_handler (handler_t::DisplayUpdate);
	script.run_handler (handler_t::CardOpen);
	hotspot.mouseMove (game::mouseCoor.x, game::mouseCoor.y);

}

void card_t::close () {
	std::cout << "closing card\n";
	script.run_handler (handler_t::CardClose);
	old_cards.push_back (this);
}

void card_t::deleteOldCards () {

	std::vector<card_t*>::iterator iter = old_cards.begin();
	while (iter != old_cards.end()) {
		if (*iter != this) {
			delete *iter;
			iter = old_cards.erase (iter);
		} else
			iter++;
	}

}

