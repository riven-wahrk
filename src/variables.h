
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_VARIABLES_H
#define RIVEN_VARIABLES_H

#include <iostream>
#include <string>
#include <map>

#include "common.h"
#include "name.h"

namespace riven {

namespace variables {

extern std::map<std::string, uint16_t> data;
extern name_t *name_res; // =1


inline void change_stack () {
	name_res = new name_t (NULL, 4);
}

void load_game (const std::string &fileName); 

inline uint16_t get (const std::string &name) {
	return data[toLower (name)];
}

inline uint16_t get (int name) {
	if (name_res != NULL)
		return get (name_res->get_string (name));
	return 0;
}

inline std::string getName (int name) {
	if (name_res != NULL)
		return name_res->get_string (name);
	return "unknown_name";
}

inline void set (const std::string &name, uint16_t val) {
	data[toLower (name)] = val;
}

inline void set (int name, uint16_t val) {
	if (name_res != NULL)
		set (name_res->get_string (name), val);
}

inline void increment (int name, uint16_t value) {
	if (name_res != NULL)
		data[toLower (name_res->get_string (name))] += value;
}


}

}

#endif

