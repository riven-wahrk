
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "mohawk.h"
#include "set.h"
#include "stack.h"

using namespace riven;


stack_t *riven::game_stack = NULL;

void mhk_group_t::load (std::vector<std::string> &files) {

	std::vector<std::string>::iterator iter;

	mohawks.reserve (files.size ());

	for (iter = files.begin(); iter != files.end(); ++iter)
		mohawks.push_back (new mohawk_t (*iter));

}

mhk_group_t::mhk_group_t (const std::string &path) {

	mohawks.push_back (new mohawk_t (path));

}


void stack_t::choose_set () {

	game_stack = new stack_t;
	
	set::init ();

}


void stack_t::load_stack (unsigned which) {

	std::vector<std::string> files;
	set::get_stack_files (which, files);

	for (int i=0; i<files.size(); i++)
		printf ("%s\n", files[i].c_str());

	game_stack->which = which;
	game_stack->load (files);

}


