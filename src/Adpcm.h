
#ifndef ADPCM_H
#define ADPCM_H

#include "Twav.h"


class AdpcmDecompress : public TwavDecompress {

	int valpred;
	int index;

	SoundBuffer *buffer;

public:

	AdpcmDecompress (SoundBuffer *in) : valpred(0), index(0), buffer(in) {}

	virtual void feedData (int8_t *in, int len);

};

#endif

