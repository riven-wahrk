
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "plst.h"

using namespace riven;

plst_t::plst_t (int id, bool drawFirst) :
	file_t (resource_t::PLST, id) {

	count = readUShort (0);

	if (drawFirst)
		draw (1);
}

void plst_t::get (int index, uint16_t *bmpId, uint16_t *left, uint16_t *top, uint16_t *right, uint16_t *bottom) {

	for (int i=0; i<count; i++) {
		if (index == readUShort (2 + 12*i)) {

			*bmpId = readUShort (2 + 12*i + 2);
			*left = readUShort (2 + 12*i + 4);
			*top = readUShort (2 + 12*i + 6);
			*right = readUShort (2 + 12*i + 8);
			*bottom = readUShort (2 + 12*i + 10);
			break;
		}
	}

}


void plst_t::draw (int record) {

	uint16_t bmpId, left, top, right, bottom;
	get (record, &bmpId, &left, &top, &right, &bottom);

	tbmp_t tbmp (bmpId);
	scene::img.draw (&tbmp, left, top, right, bottom);

}

