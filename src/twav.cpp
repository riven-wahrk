
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include "Twav.h"
#include "Game.h"

#include "TwavMP2.h"

Twav::Twav (File *fileIn) : SoundSource (NULL) {

	file = fileIn;

	fileSize = (int) file->readULong (4);
	currentChunk=0;

}


Twav::ChunkType Twav::parseCurrentChunk (int &size) {

	if (currentChunk >= fileSize)
		return DATA;

	uint32_t type = file->readULong (currentChunk);
	ChunkType chunk;
	switch (Common::swapBytes (type)) {
		case fileTypeShift ('A', 'D', 'P', 'C'):
			isMono = file->readUShort (currentChunk+10) == 1;
			size = 8 + file->readULong (currentChunk+4);
			chunk = ADPC;
			break;
		case fileTypeShift ('C', 'u', 'e', '#'):
			size = 8 + file->readULong (currentChunk+4);
			chunk = CUE;
			break;

		case fileTypeShift ('D', 'a', 't', 'a'):
			size = file->readULong (currentChunk+4);
			chunk = DATA;
			break;

		default:
			std::cout << "unknown twav chunk type: " << type << "\n";
			size = 8 + file->readULong (currentChunk+4);
			break;

	}

	std::cout << "twav chunk " << chunk << ": size " << size << std::endl;;

	return chunk;
}

char *Twav::getDataChunk (int &size) {

	if (gotoNextDataChunk () == 0)
		return NULL;

	size = file->readULong (currentChunk+4) - 28;

	char *pointer = (char*) file->readCharArray (currentChunk+28);

	return pointer;

}



Twav *Twav::create (Stack *stack, SoundMixer *mixer, int id) {

	File *file = new File (stack, Resource::tWAV, id);

	std::cout << "opening twav " << id << std::endl;

	// find out what kind of compression it uses
	Twav twav (file);

	twav.gotoNextDataChunk ();

	switch (twav.file->readUShort (twav.currentChunk+16)) {
		default:
		case 0: // pcm
//			return new TwavPCM (file, mixer);
		case 1: // adpcm
//			return new TwavADPCM (file, mixer);
		case 2: // mpeg-2 layer 2
			return new TwavMP2 (file, mixer);
	}

}



