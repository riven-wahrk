
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include "gtkmm.h"

#include "Window.h"

#include "Gtk/Draw.h"


Window::Window () {


	Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("gui.glade");

	add (box);
	Gtk::Widget *bar = refXml->get_widget ("menubar");

	bar->reparent (box);
	set_resizable (false);

	Draw *draw = new Draw ();
	box.pack_start (*draw);

	Gtk::Button *about = (Gtk::Button*) refXml->get_widget ("about");
	about->signal_clicked().connect (sigc::mem_fun (*this, &Window::openAbout));

	show_all ();

}

Window::~Window () {
}

void Window::openAbout () {
	std::cout << "openAbout\n";
}


