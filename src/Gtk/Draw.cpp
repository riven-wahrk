
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gtk/gtk.h>

#include "Draw.h"

#include "Window.h"

#include "game.h"
#include "hotspot.h"

Draw::Draw () {


	Glib::RefPtr<Gdk::GL::Config> glconfig;
	glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB | Gdk::GL::MODE_DEPTH | Gdk::GL::MODE_DOUBLE);

	if (!glconfig) {
		std::cout << "can't use opengl\n";
	}

	set_size_request (640, 480);
	set_gl_capability(glconfig);

	Glib::signal_timeout().connect(
		sigc::mem_fun(*this, &Draw::on_timeout), 10);

	add_events (Gdk::POINTER_MOTION_MASK | Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK);

}

Draw::~Draw () {
}

void Draw::on_realize() {
	Gtk::DrawingArea::on_realize();
	Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

	if (!glwindow->gl_begin(get_gl_context()))
		return;
// realize	
	



	glwindow->gl_end();
}

bool Draw::on_configure_event(GdkEventConfigure* event) {
	Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();
	if (!glwindow->gl_begin(get_gl_context()))
		return false;

// start

	riven::scene::configure (640, 480);

// stop
	glFlush ();

	glwindow->gl_end();

	return true;
}


bool Draw::on_expose_event(GdkEventExpose* event) {

	Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();

	if (!glwindow->gl_begin(get_gl_context())) {
		return false;
	}
// draw

	riven::scene::draw ();

	if (glwindow->is_double_buffered())
		glwindow->swap_buffers();
	
	else
		glFlush();
	
	glwindow->gl_end();

	return true;
}





bool Draw::on_motion_notify_event (GdkEventMotion* event) {
	
	riven::game::mouseMove (riven::Coor (event->x, event->y));
	return true;
}

bool Draw::on_button_press_event(GdkEventButton* event) {
	riven::game::mouseDown ();
	return true;
}



bool Draw::on_button_release_event(GdkEventButton* event) {
	riven::game::mouseUp ();
	return true;
}

bool Draw::on_timeout () {

	get_window()->invalidate_rect(get_allocation(), false);
	get_window()->process_updates(false);

	return true;
}







