
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef GTK_DRAW_H
#define GTK_DRAW_H


#include <iostream>

#include <gtkmm/drawingarea.h>

#include <libglademm.h>

#include <gtkglmm.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "scene.h"

class Game;

class Draw : public Gtk::DrawingArea, public Gtk::GL::Widget<Draw> {


public:
	Draw ();
	virtual ~Draw ();

protected:
	virtual void on_realize ();
	virtual bool on_configure_event (GdkEventConfigure* event);
	virtual bool on_expose_event(GdkEventExpose* event);
	virtual bool on_motion_notify_event (GdkEventMotion* event);
	virtual bool on_button_press_event(GdkEventButton* event);
	virtual bool on_button_release_event(GdkEventButton* event);
	bool on_timeout ();


};




#endif


