
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include <gtkmm/main.h>
#include <gtkmm/messagedialog.h>
#include <gtk/gtk.h>

#include "game.h"
#include "gui.h"
#include "Window.h"
#include <unistd.h>
#include "system.h"

int main (int argc, char *argv[]) {

	std::cout << "\nRiven-Wahrk, a Riven reimplementation.\n" <<
		"Copyright 2009-2010 Tyler Genter <tylergenter@gmail.com>\n" <<
		"This program comes with ABSOLUTELY NO WARRANTY\n" <<
		"This is free software, and you are welcome to redistribute it\n" <<
		"under certain conditions.\n\n" <<
		"Welcome to the Machine!\n\n";

	Gtk::Main kit (argc, argv);

	Gtk::GL::init(argc, argv);

	riven::game::reset ("/home/tyler/riven-wahrk/sub.rvn");

	Window window;

	kit.run ();
}



void riven::gui::prompt_disk (const std::string &disk_name) {

	std::string message = "Please insert the disk " + disk_name + ".";
	Gtk::MessageDialog dialog (message.c_str(), false, Gtk::MESSAGE_INFO,
		Gtk::BUTTONS_OK_CANCEL);
	dialog.run ();

}


