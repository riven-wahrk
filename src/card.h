
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_CARD_H
#define RIVEN_CARD_H

#include <iostream>
#include <vector>

#include "file.h"
#include "script.h"
#include "plst.h"
#include "hotspot.h"

namespace riven {

// be careful on how you delete this
class card_t : public file_t {


public:
	script_t script;
	plst_t plst;
	hotspot_t hotspot;

	int id;

	card_t (int idIn);

	void close ();

	// delete all the cards, except for this
	void deleteOldCards ();

	void mouseMove (Coor coor) {
		hotspot.mouseMove (coor.x, coor.y);
	};

	void mouseDown () {
		hotspot.mouseDown ();
	};

	void mouseUp () {
		hotspot.mouseUp ();
	};

	static card_t *current;

};

}

#endif

