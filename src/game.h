
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_GAME_H
#define RIVEN_GAME_H

#include <string>

#include "common.h"


namespace riven {

namespace game {

extern Coor mouseCoor;

/**
 * Starts a new game, at the beginning.
 */
void reset ();

/**
 * Starts a game based on a save file.
 *
 * @param std::string Path to the save file to be loaded.
 */
void reset (const std::string &save_game);

void mouseMove (const Coor &coorIn);
void mouseDown ();
void mouseUp ();

// this allows the user to skip the currently playing movie, such as
// by pressing the spacebar or escape
void skipMovie ();

}

}

#endif

