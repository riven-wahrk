
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>

#include "hotspot.h"
#include "game.h"

using namespace riven;

hspt_record_t::hspt_record_t (file_t *file, int addr) {

	blstId = file->readSShort (addr);
	nameRec = file->readSShort (addr + 2);
	left = file->readSShort (addr + 4);
	top = file->readSShort (addr + 6);
	right = file->readSShort (addr + 8);
	bottom = file->readSShort (addr + 10);

	mouseCursor = file->readUShort (addr + 14);
	index = file->readUShort (addr + 16);

	isZip = file->readUShort (addr + 20);
	isEnabled = true;

	script = new script_t (file, addr + 22);
	length = script->get_size ()*2 + 22; // getSize() returns number of shorts, so *2

}

hspt_record_t *hotspot_t::findRecord (uint16_t blst) {
	if (records.empty())
		return NULL;

	std::map<int,hspt_record_t>::iterator iter = records.end();

	do {
		iter--;
		if (iter->second.blstId == blst)
			return &(iter->second);
	} while (iter != records.begin());
	return NULL;
}

void hotspot_t::findRecord (int16_t x, int16_t y) {
	if (records.empty()) {
		current = NULL;
		return;
	}

	std::map<int,hspt_record_t>::iterator iter = records.end();

	do {
		iter--;
		if (iter->second.isInside(x,y))
			if ((!iter->second.isZip /* || (iter->second.isZip && game::zipMode ) FIXME*/) &&
				iter->second.isEnabled) {
				current = &(iter->second);
				return;
			}
	} while (iter != records.begin());
	current = NULL;
}

void hotspot_t::readBlst (int id) {
	file_t file (resource_t::BLST, id);

	uint16_t count = file.readUShort (0);
	for (int i=0; i<count; i++) {

		BlstRecord temp;
		temp.enabled = (file.readUShort (2 + i*6 + 2) == 1);
		temp.record = findRecord (file.readUShort (2 + i*6 + 4));
		blst.insert (std::make_pair (file.readUShort (2 + i*6), temp));

	}
}

hotspot_t::hotspot_t (int id) {


	file_t file (resource_t::HSPT, id);

	current = NULL;

	uint16_t count = file.readUShort (0);

	int offset = 2;

	for (int i=0; i<count; i++) {
		hspt_record_t temp (&file, offset);
		records.insert (std::make_pair (temp.getIndex(), temp));
		offset += temp.getLength();
	}

	readBlst (id);

}

void hotspot_t::mouseMove (int16_t x, int16_t y) {

	findRecord (x, y);
	if (!current)
		return;

	current->script->run_handler (handler_t::MouseWithin);

}

void hotspot_t::mouseDown () {
	if (current)
		current->script->run_handler (handler_t::MouseDown);
}

void hotspot_t::mouseUp () {
	if (current)
		current->script->run_handler (handler_t::MouseUp);
}

