


#include "TwavMP2.h"


bool TwavMP2::getMoreData () {

	if (inbuf)
		delete inbuf;
	
	void *temp = getDataChunk (insize);
	if (!temp)
		return false;

	inbuf = new char[insize + 2*FF_INPUT_BUFFER_PADDING_SIZE];
	memcpy (inbuf, temp, insize);
	inbuf[insize+1] = 0; // protection against corrupt mpeg streams
	inoffset = 0;

	return true;

}

bool TwavMP2::addToBuffer () {

	while (buffer.size() < 4410) {
		if (outoffset >= outsize) {
			if (!fillOutBuffer ()) { // no more data
				addSilence ();
				return false;
			}
			continue;
		}
		buffer.push (outbuf[outoffset++]);
	}
	return true;

}

// inspired by ffmpeg api examples
bool TwavMP2::fillOutBuffer () {

	if (outoffset < outsize)
		return true; // outbuf has data in it
	
	if (inoffset >= insize) {
		if (!getMoreData ())
			return false;
	}

	outsize = AVCODEC_MAX_AUDIO_FRAME_SIZE;
	int insizeTemp = insize - inoffset + 40;
	insizeTemp = (insizeTemp > 4096) ? 4096 : insizeTemp;

	int len = avcodec_decode_audio2 (c, outbuf, &outsize, (uint8_t*) inbuf + inoffset, insizeTemp);

	if (len == 0) {
		return false; // no more data
	}

	if (len < 0) {
		std::cout << "ffmpeg error decoding mpeg-2 stream\n";
		return false;
	}

	if (outsize <= 0)
		return false;

	outsize = outsize / 2;
	inoffset += len;

	outoffset = 0;
	return true;

}

TwavMP2::TwavMP2 (File *file, SoundMixer *mixer) : Twav (mixer, file) {

	avcodec_init ();
	avcodec_register_all ();

	inoffset = 0;
	insize = 0;
	inbuf = NULL;
	outoffset = 0;
	outsize = 0;

	outbuf = new int16_t[AVCODEC_MAX_AUDIO_FRAME_SIZE];

	codec = avcodec_find_decoder (CODEC_ID_MP2);
	if (!codec) {
		std::cout << "looks like ffmpeg doesn't support mpeg-2" << std::endl;
		return;
	}
	
	c = avcodec_alloc_context ();

	if (avcodec_open (c, codec) < 0) {
		std::cout << "looks like ffmpeg doesn't support mpeg-2" << std::endl;
		return;
	}
	
	currentChunk = 0;
	gotoNextDataChunk ();

	isMono = (int) file->readUChar (currentChunk + 15) == 1;

	currentChunk = 0;

	if (isMono)
		mixer->addMonoSource (this);
	else
		mixer->addStereoSource (this);

}


