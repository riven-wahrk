
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RIVEN_GUI_H
#define RIVEN_GUI_H

#include <string>

#include "common.h"

namespace riven {

namespace gui {

/**
 * Prompts the user to insert the specified disk. It returns when the user
 * clicks some button, or if the gui has some reason to believe that a disk
 * has been inserted.
 *
 * @param	disk_name The name of the disk to be inserted.
 */
void prompt_disk (const std::string &disk_name);


}

}

#endif


