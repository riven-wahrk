
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_NAME_H
#define RIVEN_NAME_H

#include <string>
#include <iostream>

#include "common.h"
#include "file.h"

namespace riven {

class name_t : public file_t {

	uint16_t count;

public:
	name_t (mhk_group_t *stack, int id) : file_t (stack, resource_t::NAME, id) {
		count = readUShort (0);
	}

	std::string get_string (int n);

	int get_count () {
		return count;
	}

	void dump () {
		for (int i=0; i<count; i++)
			std::cout << get_string (i) << "\n";
	}

};

}

#endif

