
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "system.h"


mmap_file_t *riven::mmap_file (const std::string &path) {

	mmap_file_t *f = new mmap_file_t;

	OFSTRUCT ofstruct;
	ofstruct.cBytes = sizeof (OFSTRUCT);
	HANDLE hFile = (void*) OpenFile (path.c_str(), &ofstruct, OF_READ);

	HANDLE hfileMapping = CreateFileMapping (hFile, NULL, PAGE_READONLY, 0, 0, NULL);

	// this is likely to fail, especially on a 32-bit os, since we usually
	// end up mapping a file that is 600 mb, and the address space is
	// relatively small
	f->data = (uint8_t*) MapViewOfFile (hfileMapping, FILE_MAP_READ, 0, 0, 0);
	if (!result) {
		std::string temp = "Error mapping " + path + " into address space.";

		MessageBox (NULL, temp.c_str(), "Riven-Wahrk", MB_ICONERROR);
	}

	return f; // return NULL on error

}

void riven::munmap_file (mmap_file_t *) {

	// CloseHandle
	// UnmapViewOfFile

}


void riven::list_disks (std::vector<std::string> &disks) {

	disks.clear ();

	char drive[512];
	SetErrorMode (SEM_FAILCRITICALERRORS);
	GetLogicalDriveStrings (512, drive);
	char *path = drive;
	while (path[0] != 0) {
		if (GetDriveType (path) == DRIVE_CDROM) {
			disks.push_back (std::string (path));
		}
		path += strlen (path) + 1;
	}

}


std::string riven::find_file (const std::string &dir, const std::string &path) {


	std::string res = dir + "\\" + path;

	FILE *file = fopen (res.c_str(), "r");
	if (!file)
		return "";

	fclose (file)
	return res;
}

