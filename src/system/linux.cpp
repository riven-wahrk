
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>


#include "system.h"

using namespace riven;

struct mmap_file_linux : public mmap_file_t {

	int handle;
	int size;

};

mmap_file_t *riven::mmap_file (const std::string &path) {

	mmap_file_linux *f = new mmap_file_linux;

	f->handle = open (path.c_str(), O_RDONLY);

	struct stat buf;
	fstat (f->handle, &buf);
	f->size = buf.st_size;

	f->size -= f->size % 4096;
	f->size += 4096;
	f->data = mmap (NULL, f->size, PROT_READ, MAP_PRIVATE, f->handle, 0);
	return f;

}

void riven::munmap_file (mmap_file_t *in) {

	mmap_file_linux *f = (mmap_file_linux*) in;
	munmap (f->data, f->size);
	close (f->handle);
	delete f;

}

void riven::list_disks (std::vector<std::string> &disks) {

	char drive[256];
	char crap[256];
	int intCrap;
	FILE *mtab;

	mtab = fopen ("/etc/mtab", "r");
	while (fscanf (mtab, "%s %s %s %s %d %d\n", crap, drive, crap, crap, &intCrap, &intCrap) != EOF) {
		disks.push_back (std::string (drive));
	}
	fclose (mtab);

}


std::string riven::find_file (const std::string &dirIn, const std::string &path) {

	char c_path[path.size()+10];
	std::string dir = dirIn;
	dir.reserve (dir.size() + path.size() + 2);

	strncpy (c_path, path.c_str(), path.size());
	c_path[path.size()] = 0;
	toLower (c_path);
	char *c_path_end = &c_path[path.size()];

	unsigned i;
	// split the path up into a bunch of small strings
	for (i=0; c_path[i]; i++) {
		if (c_path[i] == '/')
			c_path[i] = 0;
	}

	char *iter = &c_path[0];
	while (iter < c_path_end) {
		bool match = false;
		if (iter[0] == 0) {
			// strlen(iter) == 0;
			iter++;
			continue;
		}

		DIR *dir_handle = opendir (dir.c_str());
		dirent *entry;
		while ((entry = readdir (dir_handle)) != NULL) {
			char d_name[257];
			strncpy (d_name, entry->d_name, 256);
			d_name[256] = 0;
			toLower (d_name);
			if (!strcmp (d_name, iter)) {
				// we have a match
				dir = dir + "/" + std::string (entry->d_name);
				match = true;
				break;
			}
		}
		closedir (dir_handle);

		if (!match)
			return "";
		iter += strlen (iter) + 1;
	}

	return dir;

}


