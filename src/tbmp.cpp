
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <stdio.h>

#include <string.h>


#include "file.h"
#include "tbmp.h"

using namespace riven;

tbmp_t::tbmp_t (int id) :
	file_t (resource_t::tBMP, id) {

	width = readUShort (0);
	height = readUShort (2);
	bytesPerRow = readUShort (4);

	compressed = (readUChar (6) == 4) ? true : false;
	truecolor = (readUChar (7) == 4) ? true : false;

	std::cout << "Tbmp: id : " << id << " width: " << width << " height: " << height <<
		" bytes/row: " << bytesPerRow << std::endl;

	if (truecolor) return;

	if (compressed) {
		decompress ();
		std::cout << "compressed\n";
	} else {
		mapColorTable (readCharArray (get_size() - 784, 784));
	}

}

void tbmp_t::decompress () {

	uint8_t *data;
	data = readCharArray (get_size() - 784, 784);
	uint8_t *image;
	image = new uint8_t[bytesPerRow*height];

	int i=0, d=0, k;

	while (i<bytesPerRow*height) {

		if (data[d] == 0)
			break;

		switch (data[d] >> 6) {
			case 0:
				d++;
				for (k=data[d-1]; k>0; k--) {
					image[i++] = data[d++];
					image[i++] = data[d++];
				}
				break;

			case 1:
				d++;
				for (k=(data[d-1]) & 0x3f; k>0; k--) {
					image[i] = image[i-2]; i++;
					image[i] = image[i-2]; i++;
				}
				break;

			case 2:
				for (k=(data[d] & 0x3f); k>0; k--) {
					image[i] = image[i-4]; i++;
					image[i] = image[i-4]; i++;
					image[i] = image[i-4]; i++;
					image[i] = image[i-4]; i++;
				}
				d++;
				break;
			case 3:
				d++;
				for (k=(data[d-1] & 0x3f); k>0; k--) {
					unsigned char subCmd = data[d++];
					if (subCmd < 0x10) {
						image[i] = image[i-subCmd*2]; i++;
						image[i] = image[i-subCmd*2]; i++;
					}
					else if (subCmd == 0x10) {
						image[i] = image[i-2]; i++;
						image[i++] = data[d++];
					}
					else if (subCmd < 0x20) {
						image[i] = image[i-2]; i++;
						image[i] = image[i - (subCmd & 0xf)]; i++;
					}
					else if (subCmd < 0x30) {
						image[i] = image[i-2]; i++;
						image[i] = image[i-2] + (subCmd & 0xf); i++;
					}
					else if (subCmd < 0x40) {
						image[i] = image[i-2]; i++;
						image[i] = image[i-2] - (subCmd - 0x30); i++;
					}
					else if (subCmd == 0x40) {
						image[i++] = data[d++];
						image[i] = image[i-2]; i++;
					}
					else if (subCmd < 0x50) {
						image[i] = image[i - (subCmd & 0xf)]; i++;
						image[i] = image[i-2]; i++;
					}
					else if (subCmd == 0x50) {
						image[i++] = data[d++];
						image[i++] = data[d++];
					}
					else if (subCmd < 0x58) {
						image[i] = image[i - (subCmd - 0x50)]; i++;
						image[i++] = data[d++];
					}
					else if (subCmd == 0x58) {
						// nothing, i guess
					}
					else if (subCmd < 0x60) {
						image[i++] = data[d++];
						image[i] = image[i - (subCmd - 0x58)]; i++;
					}
					else if (subCmd < 0x70) {
						image[i++] = data[d++];
						image[i] = image[i-2] + (subCmd - 0x60); i++;
					}
					else if (subCmd < 0x80) {
						image[i++] = data[d++];
						image[i] = image[i-2] - (subCmd - 0x70); i++;
					}
					else if (subCmd < 0x90) {
						image[i] = image[i-2] + (subCmd & 0xf); i++;
						image[i] = image[i-2]; i++;
					}
					else if (subCmd < 0xa0) {
						image[i] = image[i-2] + (subCmd & 0xf); i++;
						image[i++] = data[d++];
					}
					else if (subCmd == 0xa0) {
						subCmd = data[d++];
						image[i] = image[i-2] + (subCmd >> 4); i++;
						image[i] = image[i-2] + (subCmd & 0xf); i++;
					}
					else if (subCmd == 0xb0) {
						subCmd = data[d++];
						image[i] = image[i-2] + (subCmd >> 4); i++;
						image[i] = image[i-2] - (subCmd & 0xf); i++;
					}
					else if ((subCmd >= 0xc0) && (subCmd < 0xd0)) {
						image[i] = image[i-2] - (subCmd - 0xc0); i++;
						image[i] = image[i-2]; i++;
					}
					else if ((subCmd >= 0xd0) && (subCmd < 0xe0)) {
						image[i] = image[i-2] - (subCmd - 0xd0); i++;
						image[i++] = data[d++];
					}
					else if (subCmd == 0xe0) {
						subCmd = data[d++];
						image[i] = image[i-2] - (subCmd >> 4); i++;
						image[i] = image[i-2] + (subCmd & 0xf); i++;
					}
					else if ((subCmd == 0xf0) || (subCmd == 0xff)) {
						subCmd = data[d++];
						image[i] = image[i-2] - (subCmd >> 4); i++;
						image[i] = image[i-2] - (subCmd & 0xf); i++;
					}
					else if (subCmd == 0xfc) {
						int temp1, count, position;
						temp1 = data[d++];
						count = (temp1 & 0xf8) >> 3;
						count += 2;
						position = data[d++];
						position += ((temp1 & 3) * 0x100);
						for (; count>0; count--) {
							image[i] = image[i-position]; i++;
							image[i] = image[i-position]; i++;
						}
						if ((temp1 & 4) == 0)
							image[i-1] = data[d++];
					}
					else if ((subCmd & 0xa0) == 0xa0) {
						int n=0, r=0, m;
						switch (subCmd & 0xfc) {
							case 0xa4: n=2; r=0; break; // is there a pattern?
							case 0xa8: n=2; r=1; break;
							case 0xac: n=3; r=0; break;
							case 0xb4: n=3; r=1; break;
							case 0xb8: n=4; r=0; break;
							case 0xbc: n=4; r=1; break;
							case 0xe4: n=5; r=0; break;
							case 0xe8: n=5; r=1; break;
							case 0xec: n=6; r=0; break;
							case 0xf4: n=6; r=1; break;
							case 0xf8: n=7; r=0; break;
						}
						m = data[d++];
						m += ((int) (subCmd & 3) << 8);
						for (; n>0; n--) {
							if (i >= (bytesPerRow*height))
								goto done;
							image[i] = image[i-m]; i++;
							image[i] = image[i-m]; i++;
						}
						if (r==0)
							image[i-1] = data[d++];
					}
					else {
						std::cout << "tbmp: Unknown command\n";
					}
				}
		}

	}

done:
	mapColorTable (image);
	delete[] image;

}


void tbmp_t::mapColorTable (uint8_t *data) {
	bitmap = new uint8_t[width*height*3];
	uint8_t *table = readCharArray (256, 12);

	for (int i=0; i<height; i++) {
		for (int j=0; j<width; j++) {
			bitmap[i*3*width + j*3 + 0] = table[data[i*bytesPerRow + j]*3 + 0];
			bitmap[i*3*width + j*3 + 1] = table[data[i*bytesPerRow + j]*3 + 1];
			bitmap[i*3*width + j*3 + 2] = table[data[i*bytesPerRow + j]*3 + 2];
		}
	}

}


void tbmp_t::dump (const char *name) {

	FILE *handle = fopen ("test.bmp", "r+");
	fseek (handle, 54, SEEK_SET);
	fwrite (bitmap, 608*392*3, 1, handle);
	fclose (handle);
}



void img_t::alpcpy (uint8_t *dest, uint8_t *src, int size, uint8_t a) {

	for (int i=0; i<size; i++) {

		uint32_t r = src[i] * a;
		r += dest[i] * (0xff - a);
		dest[i] = r >> 8;
	}


}


