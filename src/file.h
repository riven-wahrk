
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RIVEN_FILE_H
#define RIVEN_FILE_H

#include <string>

#include "common.h"
#include "mohawk.h" // for resource_t
#include "stack.h"

namespace riven {

class stack_t;

// each file is defined by a stack (aspit, gspit), a resource type
// (movie, script, image, sound), and a resource id

class file_t {

	uint8_t *data;
	int size;

	void load (mhk_group_t *group, uint32_t type, int id);

public:
	file_t (mhk_group_t *group, uint32_t type, int id) {
		load (group, type, id);
	}

	file_t (uint32_t type, int id) {
		load (NULL, type, id);
	}

	file_t () {};

	int get_size () {
		return size;
	}

	// dump the file to a file on the filsystem
	void dump (const std::string &path); 

	uint8_t readUChar (int addr) {
		return *(data + addr);
	}

	int8_t readSChar (int addr) {
		return *(data + addr);
	}

	uint16_t readUShort (int addr) {
		return big_to_native (*(uint16_t*) (data + addr));
	}

	int16_t readSShort (int addr) {
		return big_to_native (*(uint16_t*) (data + addr));
	}

	uint32_t readULong (int addr) {
		return big_to_native (*(uint32_t*) (data + addr));
	}

	int32_t readSLong (int addr) {
		return big_to_native (*(uint32_t*) (data + addr));
	}

	uint8_t *readCharArray (int count, int addr) {
		return data + addr;
	}

	uint16_t *readShortArray (int count, int addr);
	uint32_t *readLongArray (int count, int addr);

};

}

#endif

