
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_MOHAWK_H
#define RIVEN_MOHAWK_H

#include <string>
#include <map>

#include <stdint.h>



#include "common.h"

#include "system.h"

namespace riven {

class file_table_t {
	
	uint8_t *table;
	uint32_t count;

public:
	file_table_t (uint8_t* tableIn);

	uint32_t getSize (int index);
	uint32_t getOffset (int index);

};

#define fileTypeShift(a, b, c, d) (((d) << 24) | ((c) << 16) | ((b) << 8) | (a))

class resource_t {

public:
	static const uint32_t BLST = fileTypeShift ('B', 'L', 'S', 'T');
	static const uint32_t CARD = fileTypeShift ('C', 'A', 'R', 'D');
	static const uint32_t FLST = fileTypeShift ('F', 'L', 'S', 'T');
	static const uint32_t HSPT = fileTypeShift ('H', 'S', 'P', 'T');
	static const uint32_t MLST = fileTypeShift ('M', 'L', 'S', 'T');
	static const uint32_t NAME = fileTypeShift ('N', 'A', 'M', 'E');
	static const uint32_t PLST = fileTypeShift ('P', 'L', 'S', 'T');
	static const uint32_t RMAP = fileTypeShift ('R', 'M', 'A', 'P');
	static const uint32_t SFXE = fileTypeShift ('S', 'F', 'X', 'E');
	static const uint32_t SLST = fileTypeShift ('S', 'L', 'S', 'T');
	static const uint32_t VARS = fileTypeShift ('V', 'A', 'R', 'S');
	static const uint32_t VERS = fileTypeShift ('V', 'E', 'R', 'S');
	static const uint32_t ZIPS = fileTypeShift ('Z', 'I', 'P', 'S');
	static const uint32_t tBMP = fileTypeShift ('t', 'B', 'M', 'P');
	static const uint32_t tMOV = fileTypeShift ('t', 'M', 'O', 'V');
	static const uint32_t tWAV = fileTypeShift ('t', 'W', 'A', 'V');


private:

	uint8_t* resourceDir;
	uint8_t* resourceTable;
	uint32_t type;
	int count;

public:

	resource_t (uint8_t *resourceDirIn, uint32_t type);
	int getFileIndex (int id);

};

// Represents a single *.mhk file.

class mohawk_t {

public:

	mohawk_t (const std::string &fileName);
	~mohawk_t ();

	// returns a pointer to the file specified.
	// returns NULL if not found
	uint8_t *get_file (uint32_t type, int id);
	uint32_t get_file_loc (uint32_t type, int id);
	int get_size (uint32_t type, int id);

	std::string file_name;

private:

	mmap_file_t *file_mmap;
	uint8_t *file;
	int handle;
	uint8_t *resourceDir;
	file_table_t *fileTable;

	std::map<uint32_t,resource_t> resources;

	void loadresource_t (uint32_t type);

};

}

#endif

