
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <iostream>

#include "common.h"

#include "card.h"
#include "game.h"
#include "script.h"
#include "variables.h"

using namespace riven;

handler_t::handler_t (uint16_t *cmdListIn) {

	cmdList = cmdListIn;

	event = (handler_t::event_t) cmdList[0];

	findSegmentSize (1);

}

int handler_t::findSegmentSize (int offset) {

	int i, j = offset, k;

	j++;
	for (i=0; i<cmdList[offset]; i++) {
		if (cmdList[j] == 8) { // if statement

			j += 4;
			for (k=cmdList[j-1]; k>0; k--) {
				j++;
				j+= findSegmentSize (j);
			}

		} else {
			j += cmdList[j+1] + 2;
		}
	}

	cmdLength.insert (std::make_pair (offset, j-offset));

	return j-offset;

}



void handler_t::runSegment (int addr) {
	int i;
	addr++;
	for (i=cmdList[addr-1]; i>0; i--) {
		if (cmdList[addr] == 8) { // if statement

			bool ran = false;
			int ffff = 0;
			uint16_t val = variables::get (cmdList[addr + 2]);
			std::string name = variables::getName (cmdList[addr + 2]);
			addr += 4;

			for (int j=cmdList[addr-1]; j>0; j--) {
				// scripts are probably an array of signed shorts, in
				// which case the default case would be -1, not 0xffff
				if (cmdList[addr] == 0xffff)
					ffff = addr+1;
				addr++;
				if (cmdList[addr-1] == val) {
					std::cout << "conditional: variable " << name << " equals " << val << std::endl;
					ran = true;
					std::cout << "{\n";
					runSegment (addr);
					std::cout << "}\n";
				} else
					std::cout << "conditional: variable " << name << " (set to " << val << ") doesn't equal " << cmdList[addr-1] << std::endl;
				addr += cmdLength.find(addr)->second;
			}

			// there is a default statement
			if (!ran && (ffff != 0)) {
				std::cout << "running default branch: \n{\n";
				runSegment (ffff);
				std::cout << "}\n";
			}

		} else {

			runCmd (cmdList + addr);

			addr += 2 + cmdList[addr+1];
		}
	}
};


void handler_t::runCmd (uint16_t *cmd) {
	int temp;

	// first byte is command, second is number of args
	switch (cmd[0]) {
		case 1:
			std::cout << "cmd draw tbmp\n";
			break;
		case 2:
			if (cmd[1] == 1) {
				std::cout << "going to card " << cmd[2] << std::endl;
				card_t::current->close ();
				card_t::current = new card_t (cmd[2]);
			}
			break;
		case 3:
			std::cout << "cmd activate inline slst\n";
			break;
		case 4:
			std::cout << "cmd play local twav " << cmd[2] << "\n";
			break;
		case 7:
			std::cout << "variable " << variables::getName (cmd[2]) << " set to " << cmd[3] << std::endl;
			variables::set (cmd[2], cmd[3]);
			break;
		case 9:
			std::cout << "cmd enable hotspot\n";
			card_t::current->hotspot.enable (cmd[2]);
			break;
		case 10:
			std::cout << "cmd disable hotspot\n";
			card_t::current->hotspot.disable (cmd[2]);
			break;
		case 13:
			std::cout << "cmd set mouse cursor\n";
			break;
		case 14:
			std::cout << "cmd pause script\n";
			break;
		case 17:
			std::cout << "cmd call external command\n";
			break;
		case 18:
			if ((cmd[1] == 1) || (cmd[1] == 5)) {
				std::cout << "queing transition " << cmd[2] << std::endl;
			}
			break;
		case 19:
			std::cout << "cmd reload card\n";
			temp = card_t::current->id;
			card_t::current->close ();
			card_t::current = new card_t(temp);
			break;
		case 20:
			std::cout << "cmd disable screen update\n";
			break;
		case 21:
			std::cout << "cmd enable screen update\n";
			break;
		case 24:
			std::cout << "cmd increment variable\n";
			variables::increment (cmd[2], cmd[3]);
			break;
		case 27:
		//	throw ChangeStack (cmd[2], cmd[3], cmd[4]);
			break;
		case 32:
			std::cout << "cmd play foreground movie\n";
			break;
		case 33:
			std::cout << "cmd play background movie\n";
			break;
		case 39:
			std::cout << "cmd activate plst\n";
			card_t::current->plst.draw (cmd[2]);
			break;
		case 40:
			std::cout << "cmd activate slst\n";
			break;
		case 43:
			std::cout << "cmd activate blst\n";
			card_t::current->hotspot.activateBlst (cmd[2]);
			break;
		case 44:
			std::cout << "cmd activate flst\n";
			break;
		case 45:
			std::cout << "cmd do zip mode\n";
			break;
		case 46:
			std::cout << "cmd activate mlst\n";
			break;
		default:
			std::cout << "unknown cmd " << cmd[0] << std::endl;
	}

}



script_t::script_t (file_t *fileIn, int start) {
	cmdList = fileIn->readShortArray (fileIn->get_size () - start, start);

	int j=1;
	for (int i=0; i<cmdList[0]; i++) {
		handlers.push_back (handler_t (cmdList + j));
		j += handlers.back().getLength ();
	}
}



void script_t::run_handler (handler_t::event_t event) {

	std::list<handler_t>::iterator iter;

	for (iter = handlers.begin(); iter != handlers.end(); iter++)
		if (iter->getEventType () == event)
			iter->run ();

}



int script_t::get_size () {
	int size=1;
	std::list<handler_t>::iterator iter;
	for (iter=handlers.begin(); iter != handlers.end(); iter++)
		size += iter->getLength ();
	return size;
}



