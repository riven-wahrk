
/*
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RIVEN_SYSTEM_H
#define RIVEN_SYSTEM_H

#include <vector>
#include <string>

#include "common.h"

namespace riven {

struct mmap_file_t {

	void *data;

};

/**
 * Map the contents of the given file to some location in the address space.
 * Basically a wrapper around mmap/CreateFileMapping.
 *
 * @returns A pointer to the file's contents.
 * @param   The path to the file to be mapped.
 */
mmap_file_t *mmap_file (const std::string &path);

/** And unmap the file */
void munmap_file (mmap_file_t *);

/**
 * Lists all of the inserted disks on this computer, storing the result
 * in the given vector. Note that there will be some false positives, like
 * /proc or C:\
 */
void list_disks (std::vector<std::string> &disks);

/**
 * Performs a case-insensitive search for the given file in the given
 * directory. Returns the resulting file with an absolute path, or ""
 * if the file doesn't exist.
 */
std::string find_file (const std::string &dir, const std::string &path);

}



#endif

