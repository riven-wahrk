
#ifndef TWAVMP2_H
#define TWAVMP2_H

#include "Common.h"
#include "Twav.h"

extern "C" {
#include <libavcodec/avcodec.h>
}

class TwavMP2 : public Twav {

	AVCodec *codec;
	AVCodecContext *c;

	// compressed data, before it is fed to ffmpeg. we copy it so that 
	// it will be properly aligned and null terminated.
	char *inbuf;
	int insize, inoffset;

	// decompressed data, before it has been fed to the buffer that is
	// read by the mixer.
	int16_t *outbuf;
	int outsize, outoffset;

	bool getMoreData ();
	virtual bool addToBuffer ();

	// fills outbuf, returning false if error/no more data
	bool fillOutBuffer ();

public:
	TwavMP2 (File *file, SoundMixer *mixer);


};


#endif



