
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef __WIN32__
#include "win32/GLee.h"
#endif


#include <iostream>


#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

#include <stdio.h>

#include "game.h"
#include "hotspot.h"
#include "scene.h"

namespace riven {

static void drawHotspot ();
static void checkGLError ();

static GLuint fbTexture;

img_t scene::img (1024, 512);



void scene::configure (uint16_t width, uint16_t height) {

	std::cout << "resizing window to be " << width << " by " << height << std::endl;

	glViewport(0, 0, width, height);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluOrtho2D(0.0, (GLdouble) 640, 0.0, (GLdouble) 480);




	glGenTextures (1, &fbTexture);
	glBindTexture (GL_TEXTURE_2D, fbTexture);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB8, 1024, 512, 0, GL_BGR, GL_UNSIGNED_BYTE, img.get_bitmap ());
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glShadeModel(GL_SMOOTH);

	glFlush ();

}

#define draw_vertex(x,y) glVertex2f ( (( (x) - 304.0f) / 320.0f), (( (y) - 218.0f) / -240.0f) )

void scene::draw () {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	glEnable (GL_TEXTURE_2D);
	glBindTexture (GL_TEXTURE_2D, fbTexture);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGB8, 1024, 512, 0, GL_BGR, GL_UNSIGNED_BYTE, img.get_bitmap ());

	glBegin (GL_QUADS);
		glTexCoord2d (0, 0);
		draw_vertex (0, 0);
		glTexCoord2d (608./1024., 0);
		draw_vertex (608, 0);
		glTexCoord2d (608./1024., 392./512.);
		draw_vertex (608, 392);
		glTexCoord2d (0, 392./512.);
		draw_vertex (0, 392);
	glEnd ();

	glDisable (GL_TEXTURE_2D);

	checkGLError ();

//	drawHotspot ();

}

// green for normal, enabled hotspot
// red for normal, disabled hotspot
// blue for zip, enabled
// purple for zip, disabled


static void drawHotspot () {

#if 0
	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	std::map<int,hspt_record_t>::iterator iter;

	for (iter = game::card->hotspot.records.begin();
		iter != game::card->hotspot.records.end(); ++iter) {

		if (iter->second.isZip) {
			if (game::zipMode)
				glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
			else
				glColor4f(.75f, 0.0f, .75f, 1.0f);
		} else {
			if (iter->second.isEnabled) 
				glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
			else
				glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		}


		glBegin (GL_LINES);
			draw_vertex (iter->second.left, iter->second.top);
			draw_vertex (iter->second.right+1, iter->second.top);
			draw_vertex (iter->second.right, iter->second.top);
			draw_vertex (iter->second.right, iter->second.bottom);
			draw_vertex (iter->second.right+1, iter->second.bottom);
			draw_vertex (iter->second.left, iter->second.bottom);
			draw_vertex (iter->second.left, iter->second.bottom);
			draw_vertex (iter->second.left, iter->second.top);
		glEnd ();

	}
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
#endif

}


static void checkGLError () {
	GLenum errCode;
	const GLubyte *errString;

	if ((errCode = glGetError()) != GL_NO_ERROR) {
		errString = gluErrorString(errCode);
		fprintf (stderr, "OpenGL Error: %s\n", errString);
	}

}


}


