
// based on Charles Petzold

#include "GLee.h"

#include <windows.h>
#include <commctrl.h>

#include <gl/gl.h>
#include <gl/glu.h>

#include <iostream>

#include "Game.h"
#include "resource.h"

// no, this isn't proper c++.  i don't care

HDC hdc;
Game *game;
HINSTANCE hInst;

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

void ClientResize(HWND hWnd, int nWidth, int nHeight)
{
  RECT rcClient, rcWindow;
  POINT ptDiff;
  GetClientRect(hWnd, &rcClient);
  GetWindowRect(hWnd, &rcWindow);
  ptDiff.x = (rcWindow.right - rcWindow.left) - rcClient.right;
  ptDiff.y = (rcWindow.bottom - rcWindow.top) - rcClient.bottom;
  MoveWindow(hWnd,rcWindow.left, rcWindow.top, nWidth + ptDiff.x, nHeight + ptDiff.y, TRUE);
}

LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM) ;

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow)
{




	std::cout << "\nRiven-Wahrk, a Riven reimplementation.\n" <<
		"Copyright 2009-2010 Tyler Genter <tylergenter@gmail.com>\n" <<
		"This program comes with ABSOLUTELY NO WARRANTY\n" <<
		"This is free software, and you are welcome to redistribute it\n" <<
		"under certain conditions.\n\n" <<
		"Welcome to the Machine!\n\n";






	hInst = hInstance;

	static TCHAR szAppName[] = "riven" ;
	HWND         hwnd ;
	MSG          msg ;
	WNDCLASSEX     wndclass ;
	wndclass.cbSize        = sizeof (WNDCLASSEX);
	wndclass.style         = CS_HREDRAW | CS_VREDRAW ;
	wndclass.lpfnWndProc   = WndProc ;
	wndclass.cbClsExtra    = 0 ;
	wndclass.cbWndExtra    = 0 ;
	wndclass.hInstance     = hInstance ;
	wndclass.hIcon         = LoadIcon (NULL, MAKEINTRESOURCE (IDI_ICON1));
	wndclass.hCursor       = LoadCursor (NULL, IDC_ARROW) ;
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wndclass.lpszMenuName  = MAKEINTRESOURCE(IDR_MENU1);;
	wndclass.lpszClassName = szAppName ;
	wndclass.hIconSm = (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1),
		IMAGE_ICON, 16, 16, 0);
	
	if (!RegisterClassEx (&wndclass)) {
		MessageBox (NULL, "Error creating window.", 
			szAppName, MB_ICONERROR) ;
		return 0 ;
	}
	hwnd = CreateWindowEx (WS_EX_CLIENTEDGE,
	                       szAppName,                  // window class name
                          "Riven-Wahrk",              // window caption
								  WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_OVERLAPPED,
                         // WS_OVERLAPPEDWINDOW,        // window style
                          CW_USEDEFAULT,              // initial x position
                          CW_USEDEFAULT,              // initial y position
                          640,                        // initial x size
                          480,                        // initial y size
                          NULL,                       // parent window handle
                          NULL,                       // window menu handle
                          hInstance,                  // program instance handle
                          NULL) ;                     // creation parameters
     
	if (!hwnd)
		return 0;

	ShowWindow (hwnd, iCmdShow) ;
	UpdateWindow (hwnd) ;

	InitCommonControls ();

//	game = new Game ();
	
	while (GetMessage (&msg, NULL, 0, 0) > 0) {
		TranslateMessage (&msg) ;
		DispatchMessage (&msg) ;
	}

}


LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HGLRC hglrc;
	PAINTSTRUCT ps;
	int pf;

	switch (message) {

		case WM_CREATE:

			game = new Game ();
			hdc = GetDC (hwnd);

			static PIXELFORMATDESCRIPTOR pfd = {
				sizeof(PIXELFORMATDESCRIPTOR),          //size of structure
				1,                                      //default version
				PFD_DRAW_TO_WINDOW |                    //window drawing support
				PFD_SUPPORT_OPENGL |                    //opengl support
				PFD_DOUBLEBUFFER,                       //double buffering support
				PFD_TYPE_RGBA,                          //RGBA color mode
				32,                                     //32 bit color mode
				0, 0, 0, 0, 0, 0,                       //ignore color bits
				0,                                      //no alpha buffer
				0,                                      //ignore shift bit
				0,                                      //no accumulation buffer
				0, 0, 0, 0,                             //ignore accumulation bits
				16,                                     //16 bit z-buffer size
				0,                                      //no stencil buffer
				0,                                      //no aux buffer
				PFD_MAIN_PLANE,                         //main drawing plane
				0,                                      //reserved
				0, 0, 0 };                              //layer masks ignored

			pf = ChoosePixelFormat(hdc, &pfd);
			SetPixelFormat(hdc, pf, &pfd);

			hglrc = wglCreateContext (hdc);
			wglMakeCurrent (hdc, hglrc);

			SetTimer (hwnd, 1, 16, NULL);

			// resize it so the window itself is actually 640x480
			ClientResize (hwnd, 640, 480);

			return 0;

		case WM_SIZE:
			game->scene.configure (LOWORD (lParam), HIWORD (lParam));
			game->scene.configure (640, 480);
			return 0;

		case WM_PAINT:
			BeginPaint (hwnd, &ps);
			if (game)
				game->scene.draw ();
			glFlush ();
			SwapBuffers (hdc);
			EndPaint (hwnd, &ps);
			return 0;

		case WM_MOUSEMOVE:
			game->mouseMove (Coor (LOWORD (lParam), HIWORD (lParam)));
			return 0;

		case WM_LBUTTONDOWN:
			game->mouseDown ();
			return 0;

		case WM_LBUTTONUP:
			game->mouseUp ();
			return 0;

		case WM_DESTROY:
			PostQuitMessage (0);
			return 0;

		case WM_TIMER:
			RedrawWindow (hwnd, NULL, NULL, RDW_INTERNALPAINT);
			return 0;
		
		case WM_COMMAND:
			switch (LOWORD(wParam)) {
//				case ID_FILE_EXIT:
//					//PostQuitMessage (0);
//					PostMessage(hwnd, WM_CLOSE, 0, 0);
//					return 0;
//				case ID_HELP_ABOUT:
//					DialogBox (hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
//					return 0;
				default:
					return DefWindowProc(hwnd, message, wParam, lParam);
			}
			return 0;
	}
	return DefWindowProc (hwnd, message, wParam, lParam) ;
}


INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) {
		case WM_INITDIALOG:
			return (INT_PTR)TRUE;

		case WM_COMMAND:
			if (LOWORD (wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
				EndDialog (hDlg, LOWORD (wParam));
				return (INT_PTR)TRUE;
			}
			break;
	}
	return (INT_PTR)FALSE;
}













