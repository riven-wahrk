#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDR_MENU1                               100
#define IDI_ICON1                               101
#define IDM__ABOUT1                             40000
#define IDM__CREDITS1                           40001
#define IDM__NEW1                               40002
#define IDM__OPEN1                              40004
#define IDM__SAVE1                              40005
#define IDM_SAVE__AS1                           40006
#define IDM_E_XIT1                              40007
#define IDM__FULL_SCREEN1                       40008
#define IDM__AMBIENT_SOUNDS1                    40009
#define IDM__NONE1                              40010
#define IDM__FASTEST1                           40011
#define IDM__BEST1                              40012
