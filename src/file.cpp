
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


#include "file.h"
#include "stack.h"

using namespace riven;

void file_t::load (mhk_group_t *group, uint32_t type, int id) {

	if (!group)
		group = game_stack;

	std::vector<mohawk_t*>::iterator iter = group->mohawks.begin ();
	for (; iter != group->mohawks.end(); ++iter) {
		try {
			data = (*iter)->get_file (type, id);
			if (data) {
				size = (*iter)->get_size (type, id);
				return;
			}
		} catch (...) {}
	}

	data = NULL;


}


void file_t::dump (const std::string &path) {
#ifdef __linux__
	int handle = open (path.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
	
	write (handle, data, get_size ());
	close (handle);
#endif
}

uint16_t *file_t::readShortArray (int count, int addr) {
	uint16_t *result;

	result = new uint16_t[count];
	int i;
	for (i=0; i<count; i++)
		result[i] = readUShort (addr + 2*i);
	
	return result;
}


uint32_t *file_t::readLongArray (int count, int addr) {
	uint32_t *result;

	result = new uint32_t[count];
	int i;
	for (i=0; i<count; i++)
		result[i] = readULong (addr + 4*i);
	
	return result;
}

