
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdint.h>
#include <stdio.h>

#include "common.h"

namespace riven {

uint32_t big_to_native (uint32_t i) {

	unsigned char c1, c2, c3, c4;
	c1 = i & 255;
	c2 = (i >> 8) & 255;
	c3 = (i >> 16) & 255;
	c4 = (i >> 24) & 255;
	return ((uint32_t)c1 << 24) + ((uint32_t)c2 << 16) + ((uint32_t)c3 << 8) + c4;
}


uint16_t big_to_native (uint16_t in) {
	
	unsigned char c1, c2;
	c1 = in & 255;
	c2 = (in >> 8) & 255;
	return (c1 << 8) + c2;

}


}



