
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef TWAV_H
#define TWAV_H

#include "File.h"
#include "Sound.h"


class Twav : public SoundSource {

	Twav (File*);

protected:

	enum ChunkType { ADPC, CUE, DATA }; 

	File *file;

	int fileSize;
	// offset into the file of the chunk we are currently at. 0 if at
	// beginning of file, -1 if we are done processing. note that the first
	// chunk is always 12 bytes in.
	int currentChunk;

	// looks at the current chunk, return the type and the size
	ChunkType parseCurrentChunk (int &chunkSize);
	ChunkType parseCurrentChunk () {
		int size;
		return parseCurrentChunk (size);
	}

	// goes to the next chunk, or sets currentChunk = -1 if we are at end
	void gotoNextChunk (int size) {
		currentChunk += size;
		if (currentChunk >= fileSize)
			currentChunk = -1;
	}

	void gotoNextChunk () {
		if (currentChunk == 0) {
			currentChunk = 12;
			return;
		}

		int size;
		parseCurrentChunk (size);
		gotoNextChunk (size);
	}

	// returns the offset of the next data chunk, or 0 if we are done/there
	// is no more
	int gotoNextDataChunk () {
		if (currentChunk == -1)
			return 0;
		do {
			gotoNextChunk ();
			if (currentChunk == -1)
				return 0;
		} while (parseCurrentChunk () != DATA);
		return currentChunk;
	}

	// parses more data from the twav file, and returns it and size.
	// returns NULL when we're out
	char *getDataChunk (int &size);

	void addSilence () {
		for (int i=0; i<4410; i++)
			buffer.push (0);
	}

	virtual bool addToBuffer () { return false; };

	Twav (SoundMixer *mixer, File *fileIn) : SoundSource(mixer), file(fileIn) {}

public:

	static Twav *create (Stack *stack, SoundMixer *mixer, int id);

};




#endif

