
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>

#include "variables.h"
#include "stack.h"

using namespace riven;

std::map<std::string, uint16_t> variables::data;
name_t *variables::name_res;

// a saved game is just a mohawk. It consists of variables, with the name stored
// in the NAME resouce, and a 16-bit value stored in VARS resource.
void variables::load_game (const std::string &fileName) {

	mhk_group_t saved (fileName);
	name_t names (&saved, 1);

	file_t vars (&saved, resource_t::VARS, 1);

	for (int i=0; i<names.get_count(); i++) {
		std::string name = toLower (names.get_string (i));
		data.insert (std::make_pair (name, vars.readUShort (i*12 + 10)));
	}

}


