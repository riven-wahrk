
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_PLST_H
#define RIVEN_PLST_H

#include "file.h"
#include "tbmp.h"

namespace riven {

class plst_t : public file_t {

	int count;

	void get (int index, uint16_t *bmpId, uint16_t *left, uint16_t *top, uint16_t *right, uint16_t *bottom);

public:
	plst_t (int id, bool drawFirst = true);

	void draw (int record);

};

}

#endif

