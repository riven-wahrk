
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>

#include "card.h"
#include "game.h"
#include "set.h"
#include "stack.h"
#include "variables.h"

using namespace riven;

Coor game::mouseCoor;

namespace {

// this is true when we are currently running a script inside a card
bool insideCard;

bool ignoreMouseUp;

bool zipMode;

}

void game::reset (const std::string &save_game) {

	zipMode = false;
	insideCard = false;
	ignoreMouseUp = false;

	stack_t::choose_set ();

	stack_t::load_stack (stack_t::JSPIT);
	variables::change_stack ();

	variables::load_game ("/home/tyler/riven-wahrk/src/sub.rvn");

	card_t::current = new card_t (527);
	mouseCoor.y = mouseCoor.x = 0;
}




void game::mouseMove (const Coor &coorIn) {
	mouseCoor = coorIn;
	if (insideCard) return;
	insideCard = true;
	card_t::current->mouseMove (mouseCoor);
	insideCard = false;
}

void game::mouseDown () {
	if (insideCard) return;
	insideCard = true;
	card_t *old = card_t::current;
	card_t::current->mouseDown ();
	if (old != card_t::current)
		// we are at a new card, so ignore the next time we get a mouse
		// up signal.
		ignoreMouseUp = true;
	insideCard = false;
}


void game::mouseUp () {
	if (insideCard) return;
	if (ignoreMouseUp) {
		ignoreMouseUp = false;
		return;
	}
	insideCard = true;
	card_t::current->mouseUp ();
	insideCard = false;
}




