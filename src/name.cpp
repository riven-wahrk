
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <string.h>

#include "name.h"

using namespace riven;

std::string name_t::get_string (int n) {

	int offset = 2 + count*4 + readUShort (2 + n*2);

	std::string result = std::string ((const char*)readCharArray (get_size() - offset,  offset));

	// for some reason, there is 0xbd at the beginning and end of some strings
	std::string::size_type loc;
	while ((loc = result.find ("\275")) != std::string::npos)
		result = result.erase (loc, 1);
	
	return result;

}

