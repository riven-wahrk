
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IMG_H
#define IMG_H

#include <iostream>

#include <string.h>

namespace riven {

class img_t {

	friend class Scene;

	// copy data from src to dest, using alpha transparency and the alpha value in a
	void alpcpy (uint8_t *dest, uint8_t *src, int size, uint8_t a);

protected:
	int width, height;
	uint8_t *bitmap;

	bool isInside (int a, int b, int x) {
		return ((x >= a) && (x <= b));
	}

public:
	img_t (int w, int h) {
		std::cout << "New image " << w << "x" << h << "\n";
		width = w; height = h;
		bitmap = new uint8_t[w*h*3];
	};

	img_t () {
		bitmap = NULL;
	}

	~img_t () {
		if (bitmap)
			delete[] bitmap;
	}

	uint8_t *get_bitmap () { return bitmap; }

	// draw another image on top of this one, at x,y (coordinates are
	// from top left)
	void draw (img_t *img, int x, int y) {
		draw (img, x, y, x+img->width, y+img->height);
	}

	void draw (img_t *img, int left, int top, int right, int bottom) {
		for (int i=top; i<bottom; i++)
			memcpy (bitmap + (i*width + left)*3, img->bitmap + (i-top)*img->width*3, (right-left)*3);
	}
	

	// same as above, but draws img with alpha transparency a (which
	// is between 0 (invisible) and 255)
	void drawAlpha (img_t *img, int x, int y, uint8_t a) {
		drawAlpha (img, x, y, x+img->width, y+img->height, a);
	}

	void drawAlpha (img_t *img, int left, int top, int right, int bottom,  uint8_t a) {
		for (int i=top; i<bottom; i++)
			alpcpy (bitmap + (i*width + left)*3, img->bitmap + i*img->width*3,
				(right-left)*3, a);
	}



};

}

#endif

