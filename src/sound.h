
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SOUND_H
#define SOUND_H

#include <vector>

#define ALSA_PCM_NEW_HW_PARAMS_API
#define ALSA_PCM_NEW_SW_PARAMS_API
#include <alsa/asoundlib.h>


#include "common.h"

class SoundMixer;



// this handles everything that creates a sound, and mixes
// them all together. everything that will generate a sound
// will implement SoundSource. then there will be one instance
// of SoundMixer, that keeps track of all the SoundSource's, and
// mixes them all together, modifying them as necessary, before
// giving the resulting sound to the operating system.


struct SoundBuffer {
	int16_t buffer[2205*2*5];
	int start, end;

	void push (int16_t in) {
		buffer[end++] = in;
		if (end == 2205*2*5)
			end = 0;
	}

	int size () {
		return (end - start >= 0) ? end - start : end - start + 2205*2*5;
	}

	SoundBuffer () : start (0), end (0) {}

};

// anything that produces sound will have this class as their parent.
// this is mainly twav and tmov. they implement addToBuffer(), which
// in turn will add data to buffer.
class SoundSource {

protected:

	friend class SoundMixer;

	bool isMono;

	SoundMixer *mixer;

	SoundBuffer buffer;

	// this is called when there isn't enough data in the buffer.
	// you are expected to add more to it. returns false if there is
	// no more data, the sound should be deleted. returns true otherwise.
	virtual bool addToBuffer () = 0;

	// causes the sound to fade in.
	void fadeIn ();
	// causes the sound to fade out. when finished, it will delete itself
	void fadeOut ();

	SoundSource (SoundMixer *mixer);
	virtual ~SoundSource ();

};

// period size (in samples): 4410
// buffer size: 22050

class SoundMixer {

	friend class SoundSource;

	int16_t buffer[4410*2]; // .1 second * 2 channels

	snd_pcm_t *handle;
	snd_pcm_uframes_t buffer_size;

	std::vector<SoundSource*> monoSources, stereoSources;


	// mix two numbers together, returning the result
	int16_t mix (int16_t a, int16_t b) {
		return a+b;
	}

	// mix all the mono sources together, storing the result in buffer
	void mixMono ();
	void mixStereo ();

public:
	SoundMixer ();
	~SoundMixer ();

	// this will have to be called occasionally, to feed more data to
	// the operating system.
	void update ();

	void addMonoSource (SoundSource *source) {
		monoSources.push_back (source);
	}

	void addStereoSource (SoundSource *source) {
		stereoSources.push_back (source);
	}

	void removeSoundSource (SoundSource *source);

};



#endif


