
/*
 *
 * Riven-Wahrk - a reimplementation of the game Riven, by Cyan
 * Copyright (C) 2009-2010 Tyler Genter <tylergenter@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef RIVEN_HOTSPOT_H
#define RIVEN_HOTSPOT_H

#include <vector>
#include <map>


#include "file.h"
#include "script.h"

namespace riven {

class hotspot_t;

class hspt_record_t {

	friend class Scene;
	friend class hotspot_t;

	int16_t blstId, nameRec, left, top, right, bottom;
	uint16_t mouseCursor, index;
	bool isZip, isEnabled;

	script_t *script;

	int length;

public:
	hspt_record_t (file_t *file, int addr);
//	~hspt_record_t () { delete script; }

	int getLength () {
		return length;
	}

	int getIndex () {
		return index;
	}

	bool isInside (int16_t x, int16_t y) {
		x -= 15; y-=22;
		if ((x > left) && (x < right))
			if ((y > top) && (y < bottom))
				return true;
		return false;
	}


};


class hotspot_t {

	friend class Scene;

	hspt_record_t *current;

	std::map<int,hspt_record_t> records;

	class BlstRecord {
	public:
	
		bool enabled;
		hspt_record_t *record;
	};
	
	// first is index
	std::map<int,BlstRecord> blst;

	void readBlst (int id);
	hspt_record_t *findRecord (uint16_t blst);
	void findRecord (int16_t x, int16_t y);

public:

	hotspot_t (int id);

	void enable (int sdf) {
		std::map<int,hspt_record_t>::iterator iter = records.find(sdf);
		if (iter != records.end())
			iter->second.isEnabled = false;
	}

	void disable (uint16_t id) {
		std::map<int,hspt_record_t>::iterator iter = records.find(id);
		if (iter != records.end())
			iter->second.isEnabled = false;
	}

	void activateBlst (uint16_t id) {
		blst[id].record->isEnabled = blst[id].enabled;
	}

	void mouseMove (int16_t x, int16_t y);

	void mouseDown ();
	void mouseUp ();

};

}

#endif

